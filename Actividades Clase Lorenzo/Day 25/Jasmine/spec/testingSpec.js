describe( "Test", function () {
    it("Replace: replace each space with %20", function () {
        expect(replacesp("hola pao como estas")).toEqual("hola%20pao%20como%20estas");
    });

     it("Substring:  find if a string is substring of another", function () {
        expect(substring("hol", "ahol" )).toEqual(true);
    });

      it("Rotate:  find if a rotate string is a substring of another", function () {
        expect(rotate("prototypes", "otypesprot" )).toEqual(true);
    });


      it("ByTwo:  multiply each element of the array by two", function () {
        expect(bytwo([1,2,3])).toEqual([2,4,6]);
    });

       it("onlyEven:   evaluate if an array contains all of their elements even", function () {
        expect(onlyEven([8,2,4,1])).toEqual(false);
    });

       it("someEven:   evaluate if some element on the array is even", function () {
        expect(someEven([1,5,3])).toEqual(false);
    });

        it("givemeEven:   This function evaluates an array and returns a new one with only the even elements", function () {
        expect(givemeEven([7,6,4])).toEqual([6,4]);
    });

        it("onlyEven2:   This function evaluate if an array contains all of their elements even", function () {
        expect(onlyEven2([4,2,8])).toEqual(true);
    });

         it("givemeEven2:   This function evaluates an array and returns a new one with only the even elements", function () {
        expect(givemeEven2([7,6,4])).toEqual([6,4]);
    });


         it("ByTwo2:  multiply each element of the array by two", function () {
        expect(bytwo2([1,2,3])).toEqual([2,4,6]);
    });
   
	     it("hasEven: return true if some element of the array is even", function () {
	        expect(hasEven([1,2,3])).toEqual(true);
	    });

	     it("StringContains:  This function search if a regular expresssion is inside of a string", function () {
	      expect(Stringcontains(/beyond/,"to infinity and beyond")).toEqual(true);
	    });

	     it("Slice: This function cut an string when it finds a white space", function () {
	      expect(Slice("hola mundo ")).toEqual(["hola", "mundo"]);
	    });

	     it("Split: This function split an array when it finds a white space and convert the first char to upper case", function () {
	      expect(Split("hola mundo")).toEqual(["Hola", "Mundo"]);
	    });

	     it("Substr:This function returns the last 3 characters of a string", function () {
	      expect(Substr("hola")).toEqual("ola");
	    });

	      it("Flattening: This function use the reduce method in combination with the concat method to “flatten” an array of arrays into a single array", function () {
	      expect(flattening([[1, 2, 3], [4, 5], [6]])).toEqual([1, 2, 3, 4, 5, 6]);
	    });

	       it("deepEqual:This function takes two values and returns true only if they are the same value or are objects with the same properties ", function () {
	      expect(deepEqual({here: {is: "an"}, object: 2},{here: {is: "an"}, object: 2})).toEqual(true);
	    });
});